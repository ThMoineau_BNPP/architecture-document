ADR-XXX: Multiple Inheritance of Self-Descriptions
==================================================

:adr-id: 004
:revnumber: 0.1
:revdate: 23-04-2021
:status: draft
:author: Catalogue WP
:stakeholder: All Federation Services

Summary
-------

The Gaia-X Architecture Document states that the Self-Description schemas form
an extensible class hierarchy with inheritance of properties and relations. A
Self-Description has to state which schemas are used in its metadata. Only
properties and relations defined in these schemas must be used.

The previous paragraph implies that a Self-Description can instantiate multiple
schemas. This leaves a big design space for schemas. To foster consistency, some
general rules and normative requirements for schema design are described in this
ADR.

Discussion
----------

Suppose a hypothetical "ACME Service" that implements an SQL database service with a
REST interface and an ISO-27001 security certification.

It would require a very deep inheritance hierarchy to come up with a specialized
database service class that contains all the properties required to describe the
"ACME Service". It is furthermore not clear in which order the specializations
should be made. Such a deep inheritance hierarchy could look as follows:

   Service
     → Database Service
        → SQL Database Service
           → SQL Database Service with REST Interface
               → SQL Database Service with REST Interface and ISO27001 certification

A much cleaner approach is to model classes for individual characteristics and
to inherit from several of them. It follows a list of possible service
specialisations and the properties they define. The "ACME Service" would inherit
from all of them.

Service → Database Service → SQL Service (Capacity, Backup-Strategy, ...)
Service → Service with a REST interface (Address, TLS-Cert, Authentication-Type, ...)
Service → Service with ISO27001 certification (Level, Date, ...)

In order to foster the creation of reusable Schemas and to avoid the tendency to
create "Master Schemas" that combine many disparate characteristics, appropriate
mechanisms are needed. In a tiered approach, it seems appropriate to forbid multiple
inheritance for Gaia-X AISBL controlled schemas and allow it within an Ecosystem.
This improves the separation of concerns. There always remains the freedom for the
composition of the different characteristics at the instance level.

Decision Statements
-------------------

The Self-Description Schemas form an extensible inheritance hierarchy of RDF classes.

GAIA-X Self-Descriptions instantiate at least one and possibly multiple Self-Description
Schemas (RDF classes).

A Self-Description Schema can define mandatory properties that have to de defined by the instances.

Only attributes / properties defined in the Self-Description Schema should be used.

The Self-Description Schemas themselves do not use multiple inheritance. Only
instances of them do.

The RDF classes are segmented into namespaces. To avoid name conflicts,
the names of properties and relations can be prefixed by the namespace identifier.

Informational: The GAIA-X Foundation will define a way to assign namespaces for
domain-specific schemas to Ecosystems and/or organizations that maintain them.

Consequences
------------

A smaller number of Self-Description Schemas is required and reuse of existing
Schemas is fostered.

ADR References
--------------

* ADR-001: JSON-LD as the Exchange Format for Self-Descriptions

External References
-------------------
