# Federated Catalogue

The goal of the Federated Catalogue is to:

- enable Consumers to find best-matching offerings and to monitor for relevant changes of the offerings
- enable Producers to promote their offering while keeping full control of the level of visibility and privacy of their offerings.
- avoid a gravity effect with a lock-out and lock-in effect around a handfull of catalogues instances.

## Publication / Subscription service

For the Gaia-X catalogues to be notified about new, updated, revoked credentials a functionally speaking central Publication / Subscription service (pubsub service) must be deployed via the GXDCH instances.

The participants operating the GXDCH have a federator role.

The exact software stack to create and deploy such a service will be detailed in a further technical specifications.

It is expected that the pubsub service will have different implementations over time from a distributed service during the pilot phase - Apache Kafka or similar - to a decentralized one - a Gaia-X consortium blockchain like for the Gaia-X Registry).

The deployed solutions have to accomodate for convenience from a user point of view and for security from a federator point of view.

The credential holders are responsible to directly - via their agents or wallets - or indirectly - via a wizard, a catalogue or other - to push in the pubsub service either the credentials or the credential'IDs they wish to publish.

If the holder doesn't want to make its credential available to the wider community, the holder can decide to use another mechanism or not publish it at all.


If the holder publishes a credential, the credential is directly made available to the subcribers of the pubsub service.

If the holder publishes a credential'ID, the subcribers of the pubsub services have to resolve the ID and request access to the holder for the credential.  
The service storing the credential - agent or wallet - is from a functional point of view a Policy Decision Point (PDP) leaving full control of the credential access to the holder.

!!! note

    For privacy reasons, in case the credential is likely to contain information about a natural person, it is recommended to ONLY publish credential ID.

## Catalogue service

The implementation of the catalogue can vary from one catalogue owner to another and offer different user-experiences.

The requirement to be listed in the Gaia-X Registry as a valid Gaia-X catalogue is to keep the network of Gaia-X catalogues up-to-date by publishing in the pubsub service the credential'ID of the credential being created, updated or revoked.

A future testbed will be implemented to perform dynamic validation of the above behavior.
