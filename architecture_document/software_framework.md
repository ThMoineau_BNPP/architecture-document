# Software Framework

## Gaia-X Component Model

This chapter aims to provide the link between the Specficiations of the [Gaia-X Framework](https://docs.gaia-x.eu/framework/) and available software solutions which can be used to build Gaia-X Ecosystems.

Components which typcially make-up a Gaia-X Ecosystem are:

![Gaia-X Component Model ](figures/Gaia-X_Component_Model.png)
*Gaia-X Component Model*

Software solutions which are:

- interoperable with the Gaia-X Specifications described in the functional and technical Specifications Documents of the [Gaia-X Framework](https://docs.gaia-x.eu/framework/)

- compliant with Gaia-X rules described in the [Gaia-X Trust Framework](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/) document

- and have been validated by the Gaia-X Testbed, which will aim to automate the validation of the both points above

are listed in the Software tab of the [Gaia-X Framework](https://docs.gaia-x.eu/framework/).
