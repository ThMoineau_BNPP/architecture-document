# Gaia-X credentials

Gaia-X credentials - formely known as Self-Descriptions (_SD_) - are cryptographically signed attestation describing Entities from the Gaia-X Conceptual Model in a machine interpretable format.

The Gaia-X credentials are the building blocks of a decentralized machine readable knowledge graph of claims, each credential carrying a temper-proof and authenticable part of the information of that graph.

The knowledge graph can be completed and extended by Federations and Dataspaces which always keep control of what and how much information is being shared by implementing access-control at the credentials level.

Gaia-X credentials are [W3C Verifiable Credentials](https://www.w3.org/TR/vc-data-model/#credentials) with claims expressed in [RDF](https://www.w3.org/TR/rdf11-primer/).

The serialisation format of the claims can be done in [turtle (.ttl)](https://www.w3.org/TR/turtle/) or [JSON-LD (.json)](https://www.w3.org/TR/json-ld11/) which once serialized with [URDNA2015](https://www.w3.org/TR/rdf-canon/) for hashing and signature will output the same payload.

It is in the interest of the reader to be familiar with the [RDF data model](https://www.w3.org/TR/rdf11-primer/) to understand how Gaia-X credentials will be used in the [Gaia-X Compliance](#TODO) and how policy decisions are performed.

## Credentials lifecycle

Although a credential is a standalone document, its validity is influenced by external factors like the list of [Gaia-X Trust Anchors](#TODO) or [Compliance](#TODO) rules.  
Since credentials are immutable onced issued, their lifecycle state is therefore computed outside of the credential itself.  

A credential is a signed array of one or more claims and it can be:

- `expired` if the expiration datetime is older than current datetime or the certificate containing the key used to sign the claim has expired.
- `revoked` if the key used to sign the array is revoked. See below.
- `deprecated` if another verifiable credential with the same identifier and the same signature issuer has a newer issuance datetime.
- *user-defined* by using the [`credentialStatus`](https://www.w3.org/TR/vc-data-model/#status). **!! Given this parameter doesn't have a controlled vocabulary yet, usage of this parameter is not advised by Gaia-X. !!**
- `active` only if none of the above. **!! `active` doesn't imply Gaia-X Compliant !!**

!! note

    In Gaia-X, the credential lifecyle is managed through the lifecyle of the key used to sign the credential. As such it is recommended to use a different keypair for every credential or associated credentials to enable a granular revocation mechanism.

## Keypair lifecycle

A keypair is defined as a tuple of corresponding asymmetric encryption public and private key and can be:

- `revoked` if the key used to sign the array is revoked. Revocation status can be verified from, by priority highest to lowest:
    - the [Gaia-X Registry](#TODO)
    - if defined by the ecosystem's federators, the ecosystem local registry
    - if provided by the keypair issuer, a revocation status endpoint like [OCSP](https://datatracker.ietf.org/doc/html/rfc6960) extracted from the JWK [`x5u`](https://datatracker.ietf.org/doc/html/rfc7517#section-4.6) parameter
- `active` only if none of the above.

## Gaia-X Ontology

In order to create a knowledge graph as described above, an ontology must be defined.

An "ontology is a formal, explicit specification of a shared conceptualisation". (Gruber,1993)

In our case, it means to extract knowledge from texts in natural language and create a model that is understandable by an algorithm so one can automate the rules of the text with a computer.

The Gaia-X Ontology enables the operationalisation of Gaia-X governance through the measurement and objectivation of the services and dataset properties needed by Gaia-X participants to self-determine their level of technical, operational and legal autonomy.

The ontology is available via the [Gaia-X Registry](#TODO) hosted by the [GXDCH](#TODO).

<!--
## Credential creation

### Collecting claim

The first step is to collect Verifiable Credentials (= set of claims signed by its issuer). 
Claims can be self-signed using a keypair issued to the creator of the Credential by one of the Trust Anchors, or it can be signed by a third-party who is one of the Trust Anchors.    
The list and scope of each member of Trust Anchors are defined in the Gaia-X Trust Framework.

![](media/vc-step1.png)
*Collecting signed claims*

### Gaia-X verification

Using the collected signed claims, a participant can submit them for verification to the Gaia-X Compliance service and get in return a signed Claim with the result.  
The Gaia-X Registry and Gaia-X Compliance are developed as an Open Source project inside the Gaia-X Association.  
The version v1.0 of the services are deployed by the Gaia-X Association. The version v2.0 of those services will be distributed. The version 3.0 of those services will be decentralized.

![](media/vc-step2.png)
*Validating signed Claims using the Gaia-X Trust Framework*

### Federation governance

Using the same general workflow of the previous step, every federation is free to extend Gaia-X governance and add custom rules and checks.

![](media/vc-step3.png)
*Federation extending Gaia-X governance*
-->