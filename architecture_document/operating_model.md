# Gaia-X Operating Model

Gaia-X in its unique endeavour must have an operating model enabling a widespread adoption by small and medium-sized enterprises up to large organisations, including those in highly regulated markets, to be sustainable and scalable.

To achieve the objectives above, a non-exhaustive list of Critical Success Factors (CSFs) includes these points:

1. The operating model must provide clear and unambiguous added value to all Participants
2. The operating model must have a transparent governance and trust model with identified accountability and liability, that is clearly and fully explained to all Participants
3. The operating model must be easy to use by all Participants
4. The operating model must be financially sustainable for the Gaia-X Ecosystem
5. The operating model must be environmentally sustainable.

## Prequisites

To acheive the above goals, it is recommended for the readers to be familiar with the two documents below:

- [ISO/IEC 17000:2020 Conformity assessment](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en) definitions prepared by the ISO Committee on Conformity Assessment [CASCO](https://www.iso.org/casco.html)
- [Verifiable Credentials Data Model](https://www.w3.org/TR/vc-data-model/) definitions from the The World Wide Web Consortium (W3C)

!!! note

    Versions 22.11 and below of Gaia-X documents are refering to Verifiable Credentials Data Model 1.1. It is expected to adopt Verifiable Credentials Data Model 2.0 in later releases.

The two mentionned documents address different types of readers and the following sections will establish links between the various terms used in those documents.

### CASCO workflows

There is an overarching conformity system including one or more conformity scheme.

```mermaid
flowchart

    SystemOwner(System Owner)

    subgraph CASystem [Conformity Assessment System]
        direction TB
        SchemeOwner(Schema Owner)
        CAScheme(Conformity Assessment Scheme)
        SchemeOwner -- develops and maintains --> CAScheme
    end

    SystemOwner -- develops and maintains --> CASystem
```

## Scheme model

```mermaid
flowchart

    CAScheme(Conformity Assessment Schemes)
    criteria(Specified Requirements)
    AB(Accreditation Bodies)
    CAA(Conformity Assessment Activities)
    CAB(Conformity Assessment Bodies)
    object(Objects of Conformity Assessment)
    SchemeOwner(Schema Owners)

    CAA -- demonstrate fulfillment of --> criteria
    criteria -- need or expectation applying to --> object
    CAB -- perform --> CAA
    AB -- issue  accreditations to --> CAB
    CAScheme -- identify --> criteria
    CAScheme -- provide the methodology to perform --> CAA
    SchemeOwner -. award authority to .->  AB
    SchemeOwner -- develop and maintain --> CAScheme
```

### Roles

The ISO/IEC 17000:2020 Conformity assessment and Verifiable Credentials Data Model documents mentionned above have different but overlapping scopes. To ease the analysis, two roles must be distingued:

1. the body or party making a claim or attestation about an object.
2. the body or party cryptographically signing a claim or attestation.

While it is commonly expected that those two roles would be endorsed by the same identifiable legal or natural participant, it's not always the case, specially when the **CAB** is not capable of performing digital signatures or when the [**credentialSubject**](https://www.w3.org/TR/vc-data-model/#credential-subject) contains several `<subject> <predicate> <object>` triples as defined in [Resource Description Framework (RDF)](https://www.w3.org/TR/rdf11-primer/).

!!! note

    `<subject> <predicate> <object>` triples in [RDF](https://www.w3.org/TR/rdf11-primer/) are also called `<subject>-<property>-<value>` [claims](https://www.w3.org/TR/vc-data-model/#claims) in the Verifiable Credential Data Model.


| Scenario                                                              | ISO/IEC 17000:2020 term                                                         | Verifiable Credentials Data Model term                                       |
|-----------------------------------------------------------------------|---------------------------------------------------------------------------------|------------------------------------------------------------------------------|
| 1. the object being described.                                        | [object](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.2) | [credentialSubject](https://www.w3.org/TR/vc-data-model/#credential-subject) ([RDF object](https://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#dfn-object)) |
| 2. the body or party making a claim or attestation about an object.   | [CAB](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.6)    | [credentialSubject](https://www.w3.org/TR/vc-data-model/#credential-subject) ([RDF subject](https://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/#dfn-subject))|
| 3. the body or party cryptographically signing a claim or attestation.| [CAB](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.6)    | [issuer](https://www.w3.org/TR/vc-data-model/#issuer)                        |


### Self-Descriptions and Attestation

As seem in the previous section, an object can be described by bodies or parties having different relation with the object itself.

Gaia-X Self-descriptions is a legacy term still used in different Gaia-x documents and refers to a self-signed set of claims.  
This is equivalent to a [Verifiable Credential](https://www.w3.org/TR/vc-data-model/) where the [holder](https://www.w3.org/TR/vc-data-model/#dfn-holders) is the [issuer](https://www.w3.org/TR/vc-data-model/#dfn-issuers).

This simple approach is not enough and below is a mapping between a newer vocabulary used in the Gaia-X documents and [ISO/IEC 17000:2020](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en) definitions prepared by the ISO Committee on Conformity Assessment [CASCO](https://www.iso.org/casco.html).

| ISO/IEC 17000:2020                                                                                                  | Comment                                                | Gaia-X term used in this document                                                     |
|---------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------|---------------------------------------------------------------------------------------|
| [first-party conformity assessment activity](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.3) | self-sign claim formerly named Self-description.       | [declaration](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.5)  |
| [second-party conformity assessment activity](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.4)| claim presented to and signed by a digital Notary      | notarized declaration                                                                 |
| [third-party conformity assessment activity](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.5) | claim issued by a independant 3rd party auditor        | [certification](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.6)|

To be noted that all the terms above can be generically refered as [attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.3) and all **attestations** are issued by [**conformity assessment body (CAB)**](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.6), including declarations.

!!! note

    A **Gaia-X Self-Description** is a type of **attestation**.

### Gaia-X Trust Anchors

Gaia-X Trust Anchors are bodies, parties, ie CAB or technical means [accredited](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.7) by the bodies of the Gaia-X association to be trustworthy anchors in the cryptographic chain of keypairs used to digitally sign statements or claims about an object.

For each accredited Trust Anchors, a specific [scope of attestation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.4) is defined.

There are cases where the Trust Anchor is relative to another property in a claim.

Example: In the case of a `gx:DataResource`, the `consent` [property](https://www.w3.org/TR/vc-data-model/#dfn-property) must be signed by the `gx:Participant` identified by the `dataController` property, itself signed by the `gx:Participant` identified by the `producedBy` property, itself signed by the credential's `issuer` with a keypair from a chain of certificates with at its root a Gaia-X accredited state [Trust Service Provider](https://portal.etsi.org/TB-SiteMap/ESI/Trust-Service-Providers).  
In this example, the `dataController`'s participant, `producedBy`'s participant and the TSP are Trust Anchors because they are mandatory fixed anchors in the credential chain of signatures. In this same example, all the claims could also be signed by the `issuer` if the `dataController`'s participant,  `producedBy`'s participant and `issuer` are the same participant.


### Gaia-X Trusted Data sources and Gaia-X Notaries

When an accredited Gaia-X Trust Anchors is not capable to issue cryptographic material nor sign claim directly, then the bodies of the Gaia-X association accredit one or more Notaries which will perform a [validation](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.5) based on objective evidence from a Gaia-X Trusted Data sources and issue a [declaration](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:7.5) about the previously made attestation from the Trust Anchors.

The Notaries are not performing [audit](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.4) nor [verification](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:6.6) of the [object of conformity](https://www.iso.org/obp/ui/#iso:std:iso-iec:17000:ed-2:v2:en:term:4.2).

Example: The European Commission is providing several API, including one to check the [validity of EORI number](https://ec.europa.eu/taxation_customs/dds2/eos/eori_validation.jsp). Unfortunately, those API not returning Verifiable Credentials. Hence Gaia-X accredited the [GXDCH](https://gaia-x.eu/gxdch/) to act as Notary for EORI verification using the European Commission API as the Gaia-X Trusted Data source for EORI validation.


Example: For a CAB or set of CAB, performing third-party conformity assessment activities but not capable to digitally sign the attestations, the bodies of the Gaia-X association could accredit one or more Notaries. The role of those Notaries is to digitalize an assessment previously made. The Notaries are not performing the assessment
The **Verifiable Credential** signed by such a Notary will be second-party conformity assessment activity and have as `credentialSubject` a statement of a third-party conformity assessment activity.  
The overall trust level of such a **Verifiable Credential** will be lower than if the CAB was able to digitally sign the **Verifiable Credential** directly without involving a Notary.

## Extention by the dataspaces

The dataspaces can extend the definition of the above defined Gaia-X Trust Anchors, Gaia-X Trusted Data Sources and Gaia-X Notaries as follow:

- a federation can add more requirements on the eligibility of the Gaia-X Trust Anchors, hence selecting a subset of the Gaia-X Trust Anchors for the federation's domain.
- a federation can add additional rules on top of the ones in this document by:
    - adding more criteria for a Self-Description type. Example: adding requirements for a participant to join the federation, enforcing a deeper level of transparency for a Service Offering
    - selecting new domain specific Trust Anchors for the new criteria.

**! warning !**: For an already defined criteria, it will break Gaia-X Compliance to extend the list of Gaia-X Trust Anchors eligible to sign the criteria.

![Ecosystem rules](figures/ecosystem_rules.svg)

$$\mathbb{R}_{GaiaX} \subseteq \mathbb{R}_{domain}$$

| Rule property                                 | A domain refining Gaia-X Compliance <br>$\forall r \in \mathbb{R}_{GaiaX} \text{ and } r_{domain} \in \mathbb{R}_{domain}$ | A domain extending Gaia-X Compliance<br>$\forall r \in \mathbb{R}_{domain} \setminus \mathbb{R}_{GaiaX}$ |
|-----------------------------------------------|--------------------------------------------------------------------------------------|--------------------------------------------------------------------|
| Attribute name: $r_{name}$                           | $r_{name_{CamelCase}} \equiv r_{name_{snake\_case}}$                               | $r_{name} \notin \mathbb{R}_{GaiaX}$                             |
| Cardinality: $\lvert r \lvert$               | $\lvert r_{domain} \lvert \ge \lvert r \lvert$                                     | _no restriction_                                                   |
| Value formats: $r \rightarrow \texttt{VF}(r)$          | $\texttt{VF}(r_{domain}) \subseteq \texttt{VF}(r)$                                       | _no restriction_                                                   |
| Trust Anchors: $r \rightarrow \texttt{TA}(r)$         | $\texttt{TA}(r_{domain}) \subseteq \texttt{TA}(r)$                             | _no restriction_                                                   |
| Trusted Data Sources: $r \rightarrow \texttt{TDS}(r)$ | $\texttt{TDS}(r_{domain}) \subseteq \texttt{TDS}(r)$                 | _no restriction_                                                   |

## Gaia-X Trust Framework

The Gaia-X Trust Framework is defined as the process of going through and validating the set of automatically enforceable rules to achieve the minimum level of compatibility in terms of:

- syntactic correctness.
- schema validity.
- cryptographic signature validation.
- attribute value consistency.
- attribute value verification.

Whenever possible, the claims validation is done either by using publicly available open data, and performing tests or using data from Trusted Data Sources as defined in the previous section.
This verification is captured using Verifiable Credentials issued by either:

- the Gaia-X Trust Anchors
- the Gaia-X Notaries


However, it is expected that checking the validity of claims using data will introduce costs. In the context of the Gaia-X Ecosystem, a proposal to cover the operating cost is described later in this document with the introduction of a [Gaia-X Decentralized Autonomous Ecosystem](#gaia-x-decentralized-autonomous-ecosystem).

:information_source: Other ecosystems are autonomous and this operating model does not cover how the operating cost of ecosystems should be handled.

The set of rules is versioned and will evolve over time to adapt to legal and market requirements. Those rules are set in a separate Gaia-X Trust Framework document.[^TFdraft]

[^TFdraft]: Gaia-X Trust Framework draft: https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/

The rules will be implemented using open-source code and a service instance of that source code is called a Gaia-X Compliance Service.

**One of the first Gaia-X added values is the creation of a [FAIR](https://www.go-fair.org/fair-principles/) (findable, accessible,interoperable, reusable) decentralised knowledge graph of verifiable and composable claims.**  

## Gaia-X Labels

From the [European Data Governance Act](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:52020PC0767) proposal:

> As a compulsory scheme this could generate higher costs, which could potentially have a prohibitive impact on SMEs and startups, and the market is not mature enough for a compulsory certification scheme; therefore, lower intensity regulatory intervention was identified as the preferred policy option.  
> However, the higher intensity regulatory intervention in the form of a compulsory scheme was also identified as a feasible alternative, as it would bring significantly higher trust to the functioning of data intermediaries, and would establish clear rules for how these intermediaries are supposed to act in the European data market.

The decision for the Gaia-X Association is to adopt a compulsory scheme for Gaia-X compliance – see previous section – and an optional scheme for Gaia-X Labels, to ensure a common level of transparency and interoperability while limiting the regulatory burden on the market players.

Labels are issued for Service Offerings only and are the result of the combination of several Gaia-X credentials, that individually would be insufficient to support business or regulatory decisions.  
The issued Labels must include a version number to allow continuous evolution of the set of rules and the precise set of rules in a "rulebook" defined by the Gaia-X Association, which must include a workflow for compliance re-validation.

From a technical point of view, a Label is a [W3C Verifiable Credential](https://www.w3.org/TR/vc-data-model/#proofs-signatures).

The management of the rulebook and its governance is described in the [Gaia-X Policy Rules and Label](https://docs.gaia-x.eu) document.


### Compliance remediation

Gaia-X Compliance credentials may become invalid over time. There are three states declaring such a credential as invalid:

- Expired (after a timeout date, e.g., the expiry of a cryptographic signature)
- Deprecated (replaced by a newer Self-Description)
- Revoked (by the original issuer or a trusted party, e.g., because it contained incorrect or fraudulent information)

Expired and Deprecated can be deduced automatically based in the information already stored in the Gaia-X Registry or Gaia-X Catalogues. There are no additional processes to define. This section describes how Gaia-X credentials are revoked. 

The importance of Gaia-X compliance will grow over time, covering more and more Gaia-X principles such as interoperability, portability, and security. However, automation alone is not enough and the operating model must include a mechanism to demotivate malicious actors to corrupt the Gaia-X Registry and Gaia-X Catalogues.

The revocation of Gaia-X credentials can be done in various ways:

- **Revocation or Deprecation by authorship**: The author of a Gaia-X credentials revokes or deprecates the credential explicitly.
- **Revocation by automation**: The Gaia-X Compliance Service found at least one credential claims not validating the Gaia-X compliance rules.
- **Suspension and Revocation by manual decision**: After an audit by a compliant Gaia-X Participant, if at least one credential claims is found to be incorrect, the suspension of the Gaia-X credential is automatic. The revocation is submitted for approval to the Gaia-X Association with the opportunity for the credengtial's issuer to state its views in a matter of days. To minimize subjective decisions and promote transparency, the voting results will be visible and stored on the Gaia-X Registry or in the local Ecosystem's Registry.

## Gaia-X Decentralized Autonomous Ecosystem

The operating model described in this chapter motivates the creation of a Gaia-X decentralized autonomous Ecosystem following the principles of a Decentralized Autonomous Organisation[^dao], with the following characteristics:

- Compliance is achieved through a set of automatically enforceable rules whose goal is to incentivize its community members to achieve a shared common mission.
- Maximizing the decentralization at all levels to reduce lock-in and lock-out effects.
- Minimizing the governance and central leadership to minimize liability exposure and regulatory capture.
- The ecosystem has its own rules, including management of its own funds.
- The ecosystem is operated by the ecosystem's Participants

[^dao]: Example of the setup of a DAO <https://blockchainhub.net/dao-decentralized-autonomous-organization/>

:information_source: Other ecosystems are autonomous and this operating model does not enforce how internal ecosystem governance should be handled.

### Gaia-X Registry

The Gaia-X Registry is a public distributed, non-repudiable, immutable, permissionless database with a decentralized infrastructure and the capacity to automate code execution.

:information_source: The Ecosystems may want to have their own instance of a local Registry or equivalent. Technically, this component can be part of the ecosystem local Catalogues.

The Gaia-X Registry is the backbone of the ecosystem governance which stores information, similarly to the [Official Journal of the European Union](https://eur-lex.europa.eu/oj/direct-access.html), such as:

- the list of the Trust Anchors – keyring.
- the result of the Trust Anchors validation processes.
- the potential revocation of Trust Anchors identity.
- the vote and results of the Gaia-X Association roll call vote, similar to the rules of the [plenary of the European Parliament](https://www.europarl.europa.eu/about-parliament/en/organisation-and-rules/how-plenary-works)
- the URLs of the Gaia-X credentials schemas defined by Gaia-X
- the URLs of Gaia-X Catalogue's credentials
- … 

It also facilitates the provision of:

1. A decentralized network with smart contract functionality.
2. Voting mechanisms that ensure integrity, non-repudiation and confidentiality.
3. Access to a Gaia-X Compliance Service instance.
4. A fully operational, decentralized and easily searchable catalogue[^OP].
5. A list of Participants' identities and Self-Description URIs which violate Gaia-X membership rules. This list must be used by all Gaia-X Trusted Catalogue providers to filter out any inappropriate content.
6. Tokens may cover the operating cost of the Gaia-X Ecosystem. This specific point can be abstracted by 3rd party brokers wrapping token usage with fiat currency, providing opportunities for new services to be created by the Participants. Emitting tokens for the Gaia-X Association's members is also considered.

:information_source: Each entry in the Gaia-X Registry is considered as a transaction. A transaction contains [DID](https://w3c.github.io/did-core/)s of all actors involved in the transaction and metadata about the transaction in a machine readable format.  The basic rule for a transaction to be valid is that all DIDs have one of the Trust Anchors as root Certificate Authorities.  Please also note that the Registry stores all revoked Trust Anchors.

This model enables the Participants to operate in the Gaia-X Ecosystem, to autonomously register information, and to access the information which is verifiable by other Participants.  

[^OP]: Example of decentralized data and algorithms marketplace <https://oceanprotocol.com/>
