# Changelog

## 2022 October release (22.10)

- Update chapter on Service Composition
- Add section on Data Mesh

## 2022 September release (22.09)

- Move Self-Description technical specs to the next Identity, Credential and Access Management document
- Update Self-Description livecycle status
- Introduction of Interconnection Point Identifiers
- Introduction of new language terms for Data Exchange
- Update of the Gaia-X schemas and diagrams aligned with the [Gaia-X Framework](https://docs.gaia-x.eu/framework/)

## 2022 April release (22.04)

- Link to Trust Framework document (where the Self-Description mandatory attributes now are)
- Aligning Gaia-X architecture with NIST Cloud Federation Reference Architecture (CFRA)
- Updated definition of Data Exchange Services
- Updated Service Composition and Resource model
- Updated Self Description Lifecycle
- Consistency and alignment with other officially published Gaia-X documents, streamlining and de-duplication of text to ease reading

## 2021 December release (21.12)

- Adding `Contract` and `Computable Contract` definitions in the [Conceptual Model](conceptual_model.md)
- Update on the Self-Description lifecycle management
- Update on the Federated Trust Model

## 2021 September release (21.09)

- Rewrite of the [Operating model](operating_model.md) chapter introducing Trust Anchors, Gaia-X Compliance, Gaia-X Labels and Gaia-X Registry.
- Update of Self-Description mandatory attributes in the [Appendix](appendix.md#a3).
- Update of `Interconnection`, `Resource` and `Resource template` definitions.
- Gitlab automation improvement and speed-up
- Source available in the [21.09](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-architecture-document/-/tree/21.09) branch.

## 2021 June release (21.06)

- Adding a new [Operating model](operating_model.md) section introducing the first principle for Gaia-X governance.
- Adding preview of Self-Description mandatory attributes in the [Appendix](appendix.md#a3).
- Improvement of the [Policy rules](conceptual_model.md#policies).
- Improvement of the `Asset` and `Resource` definitions.
- Complete release automation from Gitlab.
- Source available under the [21.06](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/-/tree/21.06) tag.

## 2021 March release (21.03)

- First release of the Architecture document by the [Gaia-X Association AISBL](https://www.gaia-x.eu/)
- Complete rework of the Gaia-X [Conceptual Model](conceptual_model.md) with new entities' definition.
- Adding a [Glossary](glossary.md) section
- Source available under the [21.03-markdown](https://gitlab.com/gaia-x/gaia-x-technical-committee/gaia-x-core-document-technical-concept-architecture/-/tree/21.03-markdown) tag.

## 2020 June release (20.06)

- First release of the Technical Architecture document by the [BMWi](https://www.bmwi.de/Navigation/DE/Home/home.html)
