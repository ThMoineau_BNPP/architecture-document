# Example Gaia-X Participant Use Cases

The goal of this section is to illustrate how the Consumers, Federators
and Providers described in the conceptual model can appear in the real word.
This section focuses on the most typical kinds of actors and the list is
**not exhaustive**. Examples of Gaia-X Use Cases can be found in the [position paper published by the Dataspace Business Committee](https://www.gaia-x.eu/sites/default/files/2021-08/Gaia-X_DSBC_PositionPaper.pdf).

## Provider/Consumer Use Cases

This section describes typical kinds of Service Offering that Provider can define, offer and provide, to be configured and consumed by Consumer.

* cloud services: Infrastructure as a Service, Platform as a Service, Software as a Service, XXX as a Service.
* datasets: data sharing, in batch, stream and event driven dataset.
* software licenses: perpetual or renewable licenses for a product without an associated online service.
* interconnection & networking services that can go beyond the capacities of the regular Internet connection and exhibit special characteristics, such as and not limited to bandwidth, latency, availability or security-related settings.

## Federator Use Cases

The different Federators are not distinguished as being either domain-specific or cross-domain. Only accordance to the Policy Rules and operating according to the conditions mentioned in the Operating Model, including the respective conformity assessments and trust mechanisms, distinguish whether it is an ecosystem federated by Gaia-X policies or not.

Federators have the option to facilitate an ecosystem by using the available open source Federation Services software, such as and not limited to Catalogue, Wallet, Logging, Authorisation and Access Management

## Basic Interactions of Participants

This section describes the basic interaction of the different
Participants as described in the conceptual model (see section 2).

Providers and Consumers within a Ecosystem are identified and well
described through their valid Self-Description, which is initially
created before or during the onboarding process. Providers define their
Service Offerings
and publish them in a Catalogue. In turn, Consumers
search for Service Offerings in Gaia-X Catalogues that are coordinated
by Federators and the Gaia-X Registry. Once the Consumer finds a matching Service Offering in a
Gaia-X Catalogue, the Contract negotiation between Provider and Consumer
determines further conditions under which the Service Instance will be
provided. The Gaia-X association AISBL does not play an intermediary role during the
Contract negotiations but ensures the trustworthiness of all relevant Participants
and Service Offerings.

The following diagram presents the general workflow for Gaia-X service
provisioning and consumption processes. Please note that this overview represents
the current situation and may be subject to changes according to the
Federation Services specification. The specification will provide more
details about the different elements that are part of the concrete
processes.

![](figures/Simple_Diagram.drawio.png)

*10 Basic Provisioning and Consumption Process*
