# Overview

## Introduction

Gaia-X aims to create a federated open data infrastructure based on
European values regarding data and cloud sovereignty. The mission of
Gaia-X is to design and implement a data sharing architecture that
consists of common standards for data sharing, best practices, tools,
and governance mechanisms. It also constitutes an EU-anchored federation of cloud
infrastructure and data services, to which all 27 EU member states have
committed themselves[^1]. This overall mission drives the Gaia-X
Architecture.

[^1]: European Commission. (2020). Towards a next generation cloud for Europe. <https://digital-strategy.ec.europa.eu/en/news/towards-next-generation-cloud-europe>

This version of the Gaia-X Architecture document replaces previous versions of this document.

## Objectives

This document describes the top-level Gaia-X Architecture model. It
focuses on conceptual modelling and key considerations of an operating model.
In doing so, it aims to represent the unambiguous
understanding of the various Gaia-X stakeholder groups about the
fundamental concepts and terms of the Gaia-X Architecture in a
consistent form at a certain point in time.

It forms the foundation for further elaboration, specification, and
implementation of the Gaia-X Architecture. Thus, it creates in particular an
authoritative reference for the Gaia-X Federation Services
specification.

The Gaia-X Architecture Document is subject to continuous updates
reflecting the evolution of business requirements (e.g., from data space
activities in Europe), relevant changes in regulatory frameworks, and
advancements in the technological state of the art.

More information can be found in these resources:

* [Vision & Strategy](https://gaia-x.eu/sites/default/files/2021-12/Vision%20%26%20Strategy.pdf) document
* [Gaia-X Labeling Framework](https://gaia-x.eu/sites/default/files/2021-11/Gaia-X%20Labelling%20Framework_0.pdf)
* [Gaia-X Trust Framework](https://gaia-x.gitlab.io/policy-rules-committee/trust-framework/)

Additional information can be found at the publication section of the Gaia-X web-site:
[Gaia-X List of Publications](https://www.gaia-x.eu/publications)

## Scope

The Gaia-X Architecture document describes the concepts required to realize Gaia-X compliant data and/or infrastructure ecosystems which constitute the Gaia-X Ecosystem. It integrates the
Providers, Consumers, and Services essential for this interaction. These
Services comprise ensuring identities, implementing trust mechanisms,
and providing usage control over data exchange and Compliance -- without
the need for individual agreements.

The Gaia-X Architecture Document describes both the static decomposition
and dynamic behaviour of the Gaia-X core concepts and Federation
Services.

