# Data Exchange Services

Data Exchange in Gaia-X is enabled by a set of data exchange services that are realized by each participant and can be supported by the Federation.

| Functional needs      | Comment                                                                                              |
|-----------------------|------------------------------------------------------------------------------------------------------|
| Identity & Attributes | A common identity schema and vocabulary for attributes based access.                                 |
| Data protocols        | A common set of protocol and data format, including Enterprise Integration Patterns (EIP)            |
| Policies negotiation and contracting | A common set of Domain Specific Language (DSL) to compute access rights and usage policies           |
| Traceability          | Means to store and trace negotiation results with the capacity to log intermediate realisations, providing an auditable framekwork for transactions |
| Discoverability       | Means to search and find one or more datasets based on queries and filterings, including a common means to declare and specify data ontologies, data vocabularies, and data semantics. |

Each ecosystem is free to implement those functional needs as they see fit.

The Gaia-X Federation Services provides several tools to address those needs:

- a verifiable credential wallet: Organisation Credential Manager (OCM), Personal Credential Manager (PCM)
- policy negociation and logging: Data Contract Transaction (DCT), Data Exchange Logging (DEL)
- a catalogue: Catalogue (CAT)

### Capabilities for Exchange services

The following are essential capabilities for services and products exchange in the
data-infrastructure ecosystems:

| Capability                                        | Description |
|---------------------------------------------------|-------------|
| Expression of Policies in a machine-readable form | To enable transparency and control of service offering usage, it is important to have a common policy specification language to express usage restrictions in a formal and technology-independent manner that is understood and agreed by all Gaia-X Participants. Therefore, they must be formalized and expressed in a common Domain Specific Language, such as ODRL[^23]. |
| Inclusion of Policies in Self-Descriptions         | Informational self-determination and transparency require metadata to describe Resources as well as Providers, Consumers, and Usage Policies as provided by Self-Descriptions and the Federated Catalogues. |
| Interpretation of Usage Policies                  | For a Policy to be agreed upon, it must be understood and agreed by all Participants in a way that enables negotiation and possible technical and organizational enforcement of Policies. |
| Enforcement                                       | Monitoring of services usage is a detective enforcement of service usage with subsequent (compensating) actions. In contrast, preventive enforcement ensures the policy Compliance with technical means (e.g., cancellation or modification of data flows). |

[^23]: W3C. ODRL Information Model 2.2 [W3C Recommendation 15 February 2018]. <https://www.w3.org/TR/odrl-model/>

### Functions of Exchange Services

Information services provide more detailed information about the general
context of the services usage transactions. All information on the services
exchange and services usage transactions must be traceable;  therefore, agreed monitoring
and logging capabilities are required for all services usage transactions. Self-determination also means that Providers can choose to
apply no Usage Policies at all.

The Exchange Services in Gaia-X implement different functions
for different phases of the services exchanges. Therefore, three distinct phases of
service exchanges are defined:

-   before transaction
-   during transaction
-   after transaction

Before the service exchange transaction, the Service Agreement service is
triggered and both parties negotiate a service exchange agreement. This
includes Usage Policies and the required measures to implement those.
During transactions, a Contract Logging service receives logging-messages
that are useful to trace each transaction. This includes data provided,
data received, policy enforced, and policy-violating messages. During
and after the transaction the information stored can be queried by the
transaction partners and a third eligible party, if required.

The figure below
shows the role of the aforementionentioned services to enable controlled data exchange.

![](figures/image9.png)
*Data Services Big Picture*

`Data` are at the core of Data Exchange Services. Data are furnished by `Data Producers` (who are either data owners or data controllers in GDPR sense) to Data Providers who compose these data into a `Data Product` to be used by `Data Consumers`.

A `Data Usage Consent`, including usage terms and conditions associated with these data, is signed by both `Data Producer` and `Data Provider`, and give the `Data Provider` the legal authorization to use these data in accordance with the specified constraints. If a specific `Data License` is attached to the data (for instance when the data is liable to GDPR), then this co-signed `Data Usage Consent` constitutes a legal usage consent and must refer to the explicit license rights from the `Data Licensor` (data subject as per GDPR or data owner). Signed `Data Usage Consents` are notarized by a `Trusted Consent Authority`.

As all Gaia-X entities, `Data Products` are described by a self-description. This self-description is stored in a (searchable) `Federated Data Catalog`. Each `Data Contract` contains a `Data License` defining the usage policy for all data in this `Data Product` – it also contains other information related to billing, technical means, service level, etc. Hence a `Data Contract` constitutes a data usage contract template.

Before using a `Data Product`, the `Data Consumer` negotiate and co-sign a `Data Contrat` with the `Data Provider`. This `Data Contract` may differ from the original `Data Product Self Description`: the `Data License` of the `Data Product Self Description` is sub-licensed, possibly after modification during the negotiations, by enforceable `Terms of Usage contained` in the `Data Contract`. For each licensed data included in the `Data Product`, the `Data Contract` must include an explicit `Data Usage Consent` signed by the corresponding `Data Licensor` (in case of data liable to GDPR, the signed `Data Usage Consent` must contain all information required by the Regulation).

The `Data Contract` is a Ricardian contract: a contract at law that is both human-readable and machine-readable, cryptographically signed and rendered tamper-proof, verifiable in a decentralized fashion, and electronically linked to the subject of the contract, i.e., the data. The parties can (optionally) request this contract to be notarized.

The contract negotiation can lead to both parties agreeing on a `Data Logging Service` which is then used by both sides to log data transmission details.
