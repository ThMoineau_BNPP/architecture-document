# Federation Services

The *Federation Services* are necessary to enable a Federation of
infrastructure and data and to provide interoperability across Federations.

-   The *[inter-catalogue synchronisation](#federated-catalogue)* constitutes an indexed
    repository of Gaia-X Self-Descriptions to enable the discovery and
    selection of Providers and their Service Offerings.

-   Common vocabulary for *[Identity and Access Management](#identity-and-access-management)* covers identification, authentication and
    authorization, credential management, decentralized Identity management
    as well as the verification of analogue credentials, including between existing identity federation.

-   *[Data Exchange services](#data-exchange-services)* enable data
    exchange of Participants by providing a Data Agreement Service and a
    Data Logging Service to enable the enforcement of Policies. Furthermore,
    usage constraints for data exchange can be expressed by Provider
    Policies within the Self-Descriptions.

-   *[Gaia-X Trust Framework](#gaia-x-trust-framework)* includes mechanisms to ensure that
    Participants adhere to the Policy Rules in areas such as
    security, privacy, transparency and interoperability during
    onboarding and service delivery.

-   *[Portals and APIs](#portals-and-apis)* will support onboarding and
    management of Participants, demonstrate service discovery,
    orchestration and provisioning of sample services within ecosystems.

## The Role of Federation Services for ecosystems

The following figure visualizes how Federation Services Instances
are related to the Federator described in the conceptual model (see
section [Federator](conceptual_model.md#federator)). The Federators enable Federation Services by
obliging Federation Service Providers to provide concrete
Federation Service Instances. The sum of all Federation Service
Instances form the Federation Services.

![](figures/image12.png)
*Federation Services Relations*

### Nesting and Cascading of Federation Services

Federation Services can be nested and cascaded. Cascading is needed, for example, to ensure uniqueness of identities and Catalogue entries across different individual ecosystems / communities that use Federation Services. (Comparable to DNS servers: there are local servers, but information can be pushed up to the root servers).

Therefore, a decentralised synchronization mechanism is necessary.

### Ecosystem Governance vs. Management Operations

To enable interoperability, portability and Data Sovereignty across
different ecosystems and communities, Federation Services need to adhere
to common standards. These standards (e.g., related to service
Self-Description, digital identities, logging of data sharing
transactions, etc.) must be unambiguous and are therefore defined by the
Gaia-X Association. The Gaia-X Association owns the Trust Framework and related
regulations or governance aspects. Different entities may take on the
role of Federator and Federation Services Provider.

### Infrastructure ecosystem

The infrastructure ecosystems have a focus on computing, storage and Interconnection elements. In GAIA-X Ecosystem these elements are designated as Nodes, Interconnections and different Software Resources. They range from low-level services like bare metal computing 
up to highly sophisticated offerings, such as high-performance
computing. Interconnection Services ensure secure and performant data
exchange between the different Providers, Consumers and their services.
Gaia-X enables combinations of services that range across multiple
Providers of the ecosystem. The Interconnection Services are also the key enabler for the composition of services offered by diverse and distributed providers, ensuring the performance of single-provider networks on a multi-provider "composed" network.

### Data ecosystem

Gaia-X facilitates Data Spaces which present a virtual data integration
concept, where data are made available in a decentralised manner, for
example, to combine and share data stored in different cloud storage backends.
In general, data ecosystems enable Participants to leverage data as a strategic resource
in an inter-organizational network without restrictions of a fixed
defined partner or central keystone companies. For data to realize its
full potential, it must be made available in cross-company,
cross-industry ecosystems. Therefore, Data ecosystems not only enable
significant data value chain improvements, but provide the technical means to enable Data
Sovereignty. Such sovereign data sharing addresses different layers and
enables a broad range of business models that would otherwise be impossible. 
Trust and control mechanisms encourage the acceleration of data sharing and proliferate the growth of ecosystems.

### Federation, Distribution, Decentralization and Sharing

The principles of federation, distribution, decentralization and sharing
are emphasized in the Federation Services as they provide several
benefits for the ecosystem:


| Principle        | Need for Gaia-X          | Implemented in Gaia-X Architecture        |
|------------------|--------------------------|-------------------------------------------|
| Decentralization | Decentralization will ensure Gaia-X is not controlled by the few and strengthens the participation of the many. It also adds key technological properties like redundancy, and therefore resilience against unavailability and exploitability. Different implementations of this architecture create a diverse ecosystem that can reflect the respective requirements and strengths of its Participants.<br><br>(example: IP address assignment) | The role of Federators may be taken by diverse actors.<br><br>The open source Federation Services can be used and changed according to specific new requirements as long as they are compliant and tested. |
| Distribution     | Distribution fosters the usage of different Resources by different Providers spread over geographical locations.<br><br>(Example: Domain Name System) | Self-Description ensures that all Resources and Service Offerings are defined in standardized ways, which enables them to be listed in a searchable Catalogue, each with a unique Identifier. Therefore, it facilitates the reuse and distribution of these components. |
| Federation       | Federation technically enables connections and a web of trust between and among different parties in the ecosystem(s). It addresses the following challenges:<ul><li>Decentralized processing locations</li><li>Multiple actors and stakeholders</li><li>Multiple technology stacks</li><li>Special policy requirements or regulated markets</li></ul><br>(Example: Autonomous Systems) | Each system can interact with each other, e.g., the Catalogues could exchange information and the Identity remains unique. Furthermore, different Conformity Assessment Bodies may exist. |
| Sharing          | Sharing of the relevant services and components contributes to the Ecosystem development.<br><br>Sharing and reuse of Resources across the Gaia-X Ecosystem enables positive spillovers, leading to new and often unforeseen economic growth opportunities. | The Federated Catalogues enable the matching between Providers and Consumers. Sovereign Data Exchange lowers hurdles for data exchange and ecosystem creation. |

*Summary of Federation Services as enabler*


By utilizing common specifications and standards, harmonized rules and
policies, Gaia-X is well aligned with specifications like NIST Cloud
Federation Reference Architecture[^25]:

-   Security and collaboration context are not owned by a single entity

-   Participants in the Gaia-X Association jointly agree upon the common goals
    and governance of the Gaia-X Association 

-   Participants can selectively make some of their Resources
    discoverable and accessible by other Participants in compliance with
    Gaia-X

-   Providers can restrict their discovery and disclose certain
    information but could risk losing their Gaia-X compliance level

[^25]: Bohn, R. B., Lee, C. A., & Michel, M. (2020). The NIST Cloud Federation Reference Architecture: Special Publication (NIST SP) - 500-332. NIST Pubs. <https://doi.org/10.6028/NIST.SP.500-332>

## Interoperability and Portability for Infrastructure and Data

For the success of a Federated ecosystem it is of importance that data,
services and the underlying infrastructure can interact seamlessly with each
other. Therefore, portability and interoperability are two key
requirements for the success of Gaia-X as they are the cornerstones for
a working platform and ensure a fully functional federated, multi-provider
environment.

Interoperability is defined as the ability of several systems or
services to exchange information and to use the exchanged information
mutually. Portability refers to the enablement of data transfer and
processing to increase the usefulness of data as a strategic resource.
For services, portability implies that they can be migrated from one
provider to another, while the migration should be possible without
significant changes and adaptations and have an equivalent QoS (Quality of Service). 

### Areas of Interoperability and Portability

The Gaia-X Ecosystem includes a huge variety of Participants and Service Offerings. Therefore, interoperability needs to be ensured on different levels (Infrastructure as a Service [IaaS] , Platform as a Service [PaaS], Software as a Service [Saas], data resources, and others) by means of Service Composition.

Regarding interoperability of data, core elements to be identified in this endeavour are API specifications and best practices for semantic data descriptions. The use of semantic data interoperability is seen as a foundation to eventually create a clear mapping between domain-specific approaches based on a community process and open source efforts.

## Infrastructure and Interconnection

To best accommodate the wide variety of Service Offerings, the Gaia-X
Architecture is based on the notion of a sovereign and flexible
Interconnection of infrastructure and data ecosystems, where data is
flexibly exchanged between and among many different Participants. Therefore,
Interconnection Services represent a dedicated category of Resources as described in
section [Gaia-X Conceptual Model](conceptual_model.md).

The [Interconnection Whitepaper](https://gaia-x.eu/sites/default/files/2022-04/The%20role%20and%20the%20importance%20of%20Interconnection%20in%20Gaia-X_13042022.pdf) provides an overview on the current strategy to expand the Gaia-X architecture and federation services.





## Portals and APIs

The Portals and API support Participants to interact with 
Federation Services functions via a user interface, which provides
mechanisms to interact with core capabilities using API calls. The goal
is a consistent user experience for all tasks that can be performed with
a specific focus on security and compliance. The Portals provide
information on Resources and Service Offerings and interaction
mechanisms for tasks related to their maintenance, including identity and access management. Each ecosystem can 
deploy its own Portals to support interaction with Federation Services.
